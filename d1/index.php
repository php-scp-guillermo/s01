<?php require_once "./code.php";
//PHP code can be included from another file by using the require_once directive. 
?>

<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>S1: PHP Basics and Selection Control Structures</title>
</head>

<body>
    <h1>Echoing Values</h1>
    <p><?php echo 'Good day $name! Your given email is $email.'; ?></p>
    <?php //Double quotes can be used to print values with echo in the middle of any string 
    ?>
    <p><?php echo "Good day $name! Your given email is $email."; ?></p>
    <?php //Concatenation (Dots are used instead of plus signs): 
    ?>
    <p><?php echo 'Good day ' . $name . '!' . 'Your given email is ' . $email . '.'; ?></p>
    <p><?php echo PI; ?></p>

    <h1> Data Types</h1>
    <?php // Normal echoing of boolean and null variables in PHP will not show the values 
    ?>

    <p><?php echo $hasTraveledAbroad;  ?></p>
    <p><?php echo $spouse;  ?></p>

    <?php //To see more detailde information on the variable
    ?>
    <?php var_dump($hasTraveledAbroad); ?>
    <?php //To see their types instead, we can use gettype()
    ?>
    <p><?php echo gettype($spouse); ?></p>

    <?php // To output an array element, we can use the square bracket
    ?>
    <?php echo $grades[3] . "<br/>" ?>
    <?php echo $students[2] . "<br />" ?>
    <?php // To output an entire array, we can use print_r()
    ?>
    <pre>
    <?php print_r($students) ?>
    </pre>

    <p><?php echo $gradesObj->firstGrading; ?> /
    <p>
    <p><?php echo $personObj->address->state; ?> </p>
    <?php //echo 
    ?>
    <h1>Operators</h1>
    <h2>Assignment Operators</h2>
    <p><?= "X is $x"; ?></p>
    <p><?= "Y is $y" ?></p>
    <h2>Arithmetic Operators</h2>
    <p>Sum <?= $x + $y; ?></p>
    <p>Difference <?= $x - $y; ?></p>
    <p>Product <?= $x * $y; ?></p>
    <p>Quotient <?= $x / $y; ?></p>

    <h2>Equality Operators</h2>
    <p>Loose Equality<?= var_dump($x == '1342.14') ?></p>
    <p>Strict Equality<?= var_dump($x === '1342.14') ?></p>
    <p>Loose Inequality<?= var_dump($x != '1342.14') ?></p>
    <p>Strict Inequality<?= var_dump($x !== '1342.14') ?></p>


    <h3>Greater/Lesser Operators</h3>
    <p> Is Lesser : <?= var_dump($x < $y) ?></p>
    <p> Is Greater : <?= var_dump($x > $y) ?></p>
    <p> Is Lesser or Equal: <?= var_dump($x <= $y) ?></p>
    <p> Is Greater or Equal: <?= var_dump($x >= $y) ?></p>

    <h2>Logical Operators</h2>
    <p>Is Legal Age :<?= var_dump($isLegalAge); ?></p>
    <p>Is Registered :<?= var_dump($isRegistered); ?></p>

    <p>Are All Requirements Met: <?= var_dump($isLegalAge && $isRegistered) ?></p>
    <p>Are Some Requirements Met: <?= var_dump($isLegalAge || $isRegistered) ?></p>
    <p>Are Some Requirements Met: <?= var_dump(!$isLegalAge || !$isRegistered) ?></p>



    <h1> Functions</h1>
    <p>Full Name: <?= getFullName("John", "B", "Smith") ?></p>

    <h1>Selection Control</h1>
    <p><?= determineTyphoonIntensity("asd"); ?></p>
</body>


</html>