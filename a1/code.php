<?php
function getFullAddress(string $country, string $city, string $province, string $specificAddress): string
{
    return " <p>$country, $city, $province, $specificAddress</p>";
}


function getLetterGrade(int $grade): string
{
    if ($grade >= 98 && $grade <= 100) return "<p>$grade is equivalent to A+ </p>";

    if ($grade >= 95 && $grade <= 97) return "<p>$grade is equivalent to A </p>";

    if ($grade >= 92 && $grade <= 94) return "<p>$grade is equivalent to A- </p>";

    if ($grade >= 89 && $grade <= 91) return "<p>$grade is equivalent to B+ </p>";

    if ($grade >= 86 && $grade <= 88) return "<p>$grade is equivalent to B </p>";

    if ($grade >= 83 && $grade <= 85) return "<p>$grade is equivalent to B- </p>";

    if ($grade >= 80 && $grade <= 82) return "<p>$grade is equivalent to C+ </p>";

    if ($grade >= 77 && $grade <= 79) return "<p>$grade is equivalent to C </p>";

    if ($grade >= 75 && $grade <= 76) return "<p>$grade is equivalent to C- </p>";

    if ($grade < 75) return "<p>$grade is equivalent to F </p>";
}
