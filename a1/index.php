<?php
require_once "./code.php";
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>
        div {
            padding: 1rem;
            border: 5px solid;
            width: 500px;
            margin: 0 auto;
        }
    </style>
</head>

<body>

</body>
<div>
    <h1>Full Address</h1>
    <?= getFullAddress(
        "3F Caswynn Bldg.",
        "Timog Aveneu",
        "Quezon City",
        "Metro Manila Philippines"
    ); ?>
</div>
<br />

<div>
    <h1>Letter-Based Grading</h1>
    <?= getLetterGrade(98); ?>
    <?= getLetterGrade(95); ?>
    <?= getLetterGrade(92); ?>
    <?= getLetterGrade(89); ?>
    <?= getLetterGrade(86); ?>
    <?= getLetterGrade(83); ?>
    <?= getLetterGrade(80); ?>
    <?= getLetterGrade(77); ?>
    <?= getLetterGrade(75); ?>
    <?= getLetterGrade(74); ?>
</div>


</html>